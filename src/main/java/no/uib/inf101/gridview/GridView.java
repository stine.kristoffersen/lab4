package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.GridDimension;
import no.uib.inf101.colorgrid.IColorGrid;


public class GridView extends JPanel{
  // TODO: Implement this class
  private IColorGrid grid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
  private CellPositionToPixelConverter mix;

  public GridView(IColorGrid grid) {

    this.grid = grid;

    this.setPreferredSize(new Dimension(400, 300));
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    drawGrid(g2);

  }

  
  private void drawGrid(Graphics2D g2) {

    // Et rektangel som alltid har 15px avstand til kantene på lerretet
    double margin = OUTERMARGIN;
    double x = margin;
    double y = margin;
    double width = this.getWidth() - 2 * margin;
    double height = this.getHeight() - 2 * margin;
    Rectangle2D box = new Rectangle2D.Double(x, y, width, height);
    g2.setColor(MARGINCOLOR.darker().darker());
    g2.fill(box);
    


    CellPositionToPixelConverter info = new CellPositionToPixelConverter(box, grid, margin);
    drawCells(g2, grid, info);
  
  }

  private static void drawCells(Graphics2D g2, CellColorCollection cc, CellPositionToPixelConverter cp) {

    for (CellColor posColor : cc.getCells()) {
      Color farge = posColor.color();
      if (posColor.color() == null) {
        farge = Color.DARK_GRAY;
      }
      
      Rectangle2D box = cp.getBoundsForCell(posColor.cellPosition());
      g2.setColor(farge);
      g2.fill(box);
    }

  }

}
