package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {
  // TODO: Implement this class

  private Rectangle2D box;
  private GridDimension gd;
  private double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }


  public Rectangle2D getBoundsForCell(CellPosition cp) {
    // skal finne x, y, bredde og høyde til celle

    double cellWidth = (box.getWidth() - (gd.cols() + 1)*margin)/gd.cols();
    double cellHight = (box.getHeight() - (gd.rows() + 1)*margin)/gd.rows();
    double cellX = box.getX() + (cp.col() + 1)*margin + cp.col()*cellWidth;
    double cellY = box.getY() + (cp.row() + 1)*margin + cp.row()*cellHight;

    Rectangle2D.Double ny = new Rectangle2D.Double(cellX, cellY,cellWidth, cellHight);

    return ny;
  }
}
