package no.uib.inf101.colorgrid;

import java.util.List;
import java.util.ArrayList;
import java.awt.Color;

public class ColorGrid implements IColorGrid{

    private int rows;
    private int cols;
    private List<List<Color>> grid; // skal inneholde elementer med cellPosition og color

    public ColorGrid(int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
    this.grid = new ArrayList<>();
    for (int i=0; i < rows; i++) { // går gjennom radene
            List<Color> rowsAsList = new ArrayList<>(); // for hver rad lages en ny liste rowsAsList
            for (int y=0; y < cols; y++) { // går gjennom kolonnene
                rowsAsList.add(null);
            }
            this.grid.add(rowsAsList);
        }
    }

    @Override
    public List<CellColor> getCells() {
        List<CellColor> listOfCells = new ArrayList<>();
        for (int i=0; i<this.rows(); i++) {
            for (int j = 0; j < this.cols; j++) {
                CellPosition pos = new CellPosition(i, j);
                Color color = this.grid.get(i).get(j);
                CellColor posColor = new CellColor(pos, color);
                listOfCells.add(posColor);
                
            }
        }
        return listOfCells;
    }
    
    @Override
    public int rows() {
        return this.rows;
    }
    
    @Override
    public int cols() {
        return this.cols;
    }

    /**
   * Get the color of the cell at the given position.
   *
   * @param pos the position
   * @return the color of the cell
   * @throws IndexOutOfBoundsException if the position is out of bounds
   */
    @Override
    public Color get(CellPosition pos) {
        Color color = this.grid.get(pos.row()).get(pos.col());
        return color;
    }



    /**
   * Set the color of the cell at the given position.
   *
   * @param pos the position
   * @param color the new color
   * @throws IndexOutOfBoundsException if the position is out of bounds
   */
    @Override
    public void set(CellPosition pos, Color color) {
        this.grid.get(pos.row()).set(pos.col(), color);
    }
}